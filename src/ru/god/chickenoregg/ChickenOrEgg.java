package ru.god.chickenoregg;

/**
 * Класс, предназначенный для создания потоков, осуществляющих "спор"
 *
 * @author Горбачева, 16ИТ18к
 */
public class ChickenOrEgg extends Thread {
    private String name;

    ChickenOrEgg(String name) {
        this.name = name;
    }

    /**
     * Метод, реализующий вывод строки с задержкой 1 сек
     */
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name);
        }
    }
}
