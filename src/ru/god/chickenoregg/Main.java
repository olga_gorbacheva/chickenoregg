package ru.god.chickenoregg;

/**
 * Класс, реализующий "спор" двух потоков
 *
 * @author Горбачева, 16ИТ18к
 */
public class Main {
    public static void main(String[] args) {
        ChickenOrEgg chicken = new ChickenOrEgg("Курица");
        ChickenOrEgg egg = new ChickenOrEgg("Яйцо");
        chicken.start();
        egg.start();
        while (chicken.isAlive() || egg.isAlive()) {
            try {
                chicken.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (egg.isAlive()) {
                try {
                    egg.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Победило яйцо");
            } else {
                System.out.println("Победила курица");
            }
        }
    }
}
